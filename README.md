# free-orgcss

A Project that provides CSS style for Org exported HTML file.

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY.

## 介绍

本项目是一款为 Org-export 生成的HTML文档提供CSS样式的软件。

本项目受[gongzhitaao/orgcss](https://github.com/gongzhitaao/orgcss)启发创作。

我之前在自己的个人网站使用了 gongzhitaao/orgcss 项目，但是很遗憾的是，该项目使用的 [Anti 996 License](https://github.com/kattgu7/Anti-996-License) 并不是自由软件许可证。

所以我开发了这样一个自由软件项目，旨在替代 gongzhitaao/orgcss .

### 为什么要这样做？你难道不支持反996运动吗？

我在自己的个人网站中给出了[解释](https://ilgharkus.github.io/copyleft.html#orgc5571be)：

>我支持反996运动，但是我同时也尊重软件用户与文档读者的自由。 我虽然在感情上不希望任何侵犯员工劳动权益的实体使用我的软件或文档，但是为了保证自己的自由，我同样也要支持并尊重其他所有人的自由。 

